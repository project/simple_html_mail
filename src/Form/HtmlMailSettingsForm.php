<?php

namespace Drupal\simple_html_mail\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class HtmlMailSettingsForm.
 */
class HtmlMailSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a HtmlMailSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'simple_html_mail.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'html_mail_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('simple_html_mail.settings');

    $image = $config->get('image');
    $form['image'] = [
      '#type' => 'managed_file',
      '#upload_location' => 'public://html-mail-template',
      '#title' => $this->t('Image'),
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg jpeg png gif'],
        'file_validate_size' => [25600000],
      ],
      '#default_value' => isset($image) ? $image : '',
      '#description' => $this->t('The logo image to display in email'),
      '#required' => TRUE,
    ];

    $form['header_slogan'] = [
      '#title' => $this->t('Header slogan text'),
      '#type' => 'textfield',
      '#default_value' => $config->get('header_slogan'),
    ];

    $form['footer_text'] = [
      '#title' => $this->t('Footer text'),
      '#type' => 'textfield',
      '#default_value' => $config->get('footer_text'),
    ];

    $form['mail_ids'] = [
      '#title' => $this->t('Mail IDs'),
      '#type' => 'textarea',
      '#default_value' => $config->get('mail_ids'),
      '#description' => $this->t('Enter comma separated e-mail ids for e-mails that you want to go through HTML mail template.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save image as permanent.
    $image = $form_state->getValue('image');
    if ($image != $this->config('simple_html_mail.settings')->get('image')) {
      if (!empty($image[0])) {
        $file = $this->entityTypeManager->getStorage('file')->load($image[0]);
        $file->setPermanent();
        $file->save();
      }
    }

    $this->config('simple_html_mail.settings')
      ->set('mail_ids', $form_state->getValue('mail_ids'))
      ->set('image', $form_state->getValue('image'))
      ->set('header_slogan', $form_state->getValue('header_slogan'))
      ->set('footer_text', $form_state->getValue('footer_text'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

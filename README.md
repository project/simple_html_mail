Simple HTML mail module
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

This module allows you to send HTML emails that are properly styled through the email template.

After enabling module use "simple_html_mail_send" function for sending mails from your custom modules.

REQUIREMENTS
------------

This module requires listed modules in order to be installed:
 * Mail system
 * Swift mailer

INSTALLATION
------------

Install the module as you would normally install a contributed Drupal module. After installing the module you need to configure required modules:
 * Mail system
   ```
   Go to "admin/config/system/mailsystem"
   Set "Swift Mailer" as Formatter and Sender.
   ```
   
 * Swift mailer
   ```
   Go to "admin/config/swiftmailer/messages"
   Set HTML as Message format.
   ```

CONFIGURATION
--------------

Go to "admin/config/system/html-mail-settings" for the configuration screen. The available configurations are:
 * The logo image to display in email
 * Header slogan text
 * Footer text
 * Enter e-mail id for e-mails that you want to go through HTML mail template to alter them. 

MAINTAINERS
-----------

This module was created by <a href="https://www.drupal.org/u/darko-zaric" target="_blank">Darko Zaric</a> from <a href="https://www.drupal.org/eton-digital" target="_blank">Eton Digital</a>.

